//Leer el precio de 7 artículos, sumarlos y mostrar el total*\
//ciclo FOR

import javax.swing.JOptionPane;

public class SumaArticulos{
public static void main (String arg[])
{
	int precio, sumatoria, cant;
	String dato, lista;
	sumatoria=0;
	lista="La lista de precios es: \n";
	
	for(cant=1;cant<=7;cant++) //++ (cant=cant+1;) cant=cant+2---> cant+=2 
	{ 
		dato=JOptionPane.showInputDialog("Digite el precio del articulo"+cant);
		precio=Integer.parseInt(dato);
		
		sumatoria=sumatoria+precio; //sumatoria+=precio;
		lista+= cant+"-"+precio+"\n";
		
	 }//fin del ciclo FOR 
	
	JOptionPane.showMessageDialog(null, lista+"\nEl resultado es "+sumatoria);
	
	
    }//fin método main
 

}//fin de la clase
