public class Persona{
	String nombre;
	String fechaNacimiento;
	
	public void setNombre(String nombrePersona){
	   nombre=nombrePersona;	
	}
	
	public String getNombre(){
	   return nombre;	
	}
	
	public void setFechaNacimiento(String fecha){
	   fechaNacimiento=fecha;	
	}
	
	public String getFechaNacimiento(){
	   return fechaNacimiento;	
	}
}
