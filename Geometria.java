import javax.swing.JOptionPane;
public class Geometria
{
	public static void main(String arg[])
	{
		Rectangulo rectangulo=new Rectangulo();
		String datoLeido, continuar;
		int largo, ancho;
		continuar= ("si");
		while (continuar.equalsIgnoreCase ("si"))
		{
			datoLeido=JOptionPane.showInputDialog("Digite el ancho del rectangulo");
		    ancho=Integer.parseInt(datoLeido);
		    rectangulo.setAncho(ancho);
		    datoLeido=JOptionPane.showInputDialog("Digite el largo del rectangulo");
		    largo=Integer.parseInt(datoLeido);
		    rectangulo.setLargo(largo);
		    JOptionPane.showMessageDialog(null,"El perímetro del rectangulo que su ancho mide "+rectangulo.getAncho()+" y su largo "+rectangulo.getLargo()+" es " +rectangulo.calcularPerimetro());
		    JOptionPane.showMessageDialog(null,"El área del rectangulo que su ancho mide "+rectangulo.getAncho()+" y su largo "+rectangulo.getLargo()+" es " +rectangulo.calcularArea());
		    datoLeido=JOptionPane.showInputDialog("Si desea continuaer digite SI, sino, digite NO");
	        continuar=(datoLeido);
		}
	}
} 
