Proceso sin_titulo
	Definir elemento, menorA, mayorA, porceMayor, porceMenor, far, cel, monitoreo Como Real;
	Definir mensaje, dia Como Caracter;
	mensaje="";
	mayorA=0;
	menorA=0;
	Escribir "Digite la cantidad de d�as a monitorear";
	Leer monitoreo;
	Para elemento<-1 Hasta monitoreo Con Paso 1 Hacer
		Escribir "Digite el d�a";
		Leer dia;
		Escribir "Digite la temperatura del medio d�a";
		Leer far;
		cel=((far-32)*5)/9;
		Si cel<=29 Entonces
			menorA=menorA+1;
		SiNo
			mayorA=mayorA+1;
		FinSi
	FinPara
	porceMenor=(menorA/monitoreo)*100;
	porceMayor=(mayorA/monitoreo)*100;
	Escribir "El total de temperaturas iguales o menores a 29 celsius es de ",menorA,", y las temperaturas mayores a 29 celsius es de ",mayorA;
	Escribir "El porcentaje de las temperaturas menores o iguales a 29 celsius, con respecto al monitoreo es de ",porceMenor,"%, y el porcentaje de las que son mayores a 29 celsius, con respecto al monitoreo es de ",porceMayor,"%";
FinProceso
