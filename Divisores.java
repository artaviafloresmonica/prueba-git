/*A partir de un n�mero digitado por un usuario, determine cu�les son los 
divisores de ese n�mero
6= 1 2 3 6 
12= 1 2 3 4 6 12 */
import javax.swing.JOptionPane;
public class Divisores{
  public static void main(String arg[]){
    int numero,residuo;
    String listaDivisores,datoLeido;
    //Inicializar variable que acumula
    listaDivisores=" La lista de divisores del n�mero es: \n1";
    datoLeido=JOptionPane.showInputDialog("Digite un n�mero para obtener sus divisores");
    numero=Integer.parseInt(datoLeido);
    for(int divisor=2;divisor<=numero;divisor++){
      residuo=numero%divisor;//% es equivalente al mod de PSeInt
      if (residuo==0){
        listaDivisores+="\n"+divisor;
      }//Fin del if
      
    }//Fin del for
  JOptionPane.showMessageDialog(null,listaDivisores);
  }//Fin del main

}//Fin de la clase