/*La compañía de autobuses Curvas peligrosas requiere determinar el costo que
tendrá el pasaje de un viaje. La empresa cobra basada en los kilómetros por recorrer y
en el costo por kilómetro. Realice un programa para resolver este problema. Tanto la
entrada como la salida debe ser por medio de un cuadro de diálogo.*/

import javax.swing.JOptionPane;
public  class Buses{
	public static void main (String srg[]){
	
	int precio, kilome;
	double pagaTotal;
	String datoLeido;
	
	datoLeido=JOptionPane.showInputDialog("Digite el costo por kilometro");
	precio=Integer.parseInt(datoLeido);
	
	datoLeido=JOptionPane.showInputDialog("Digite los kilometros recorridos");
	kilome=Integer.parseInt(datoLeido);
	
	pagaTotal=precio*kilome;
	
	JOptionPane.showMessageDialog(null,"El total a pagar es de "+pagaTotal+" colones");
		}//Fin del main
}//Fin de la Clase

