import javax.swing.JOptionPane;
public class Juego{
	private Estadistica estadistica;
	
	public Juego(){
	estadistica=new Estadistica();
	menu();
	}
	
	public static void main(String arg[]){
	Juego juego=new Juego();	
	}
	
	public void menu(){
	Estadistica estadistica=null;
	String datoLeido, nombre;
	int opcion;
	opcion=1;
	nombre=JOptionPane.showInputDialog("Digite el nombre con el que desea jugar");
	do{
		datoLeido=JOptionPane.showInputDialog("************* Menú ************"
                                             +"\n1. Reiniciar estadística"
                                             +"\n2. Reiniciar jugador"
                                             +"\n3. Jugar"
                                             +"\n4. Mostrar resultado"
                                             +"\n5. Salir");
        opcion=Integer.parseInt(datoLeido);
        switch(opcion){
			case 1:
			if(estadistica!=null){
				estadistica= new Estadistica();
				JOptionPane.showMessageDialog(null,"Sus estadísticas han sido reiniciadas");
				}
				else{
					JOptionPane.showMessageDialog(null,"No tiene estadísticas registradas");
					}
			break;
			
			
			case 2:
			if(estadistica!=null){
				estadistica= new Estadistica();
				nombre=JOptionPane.showInputDialog("Digite el nombre con el que desea jugar");
				
			}
			else{ 
				JOptionPane.showMessageDialog(null,"No tiene estadísticas registradas");
			}
			break;
			
			
			case 3:
			jugar();
			break;
			
			case 4:
			JOptionPane.showMessageDialog(null,nombre);
			mostrarEstadistica();
			break;
			
			case 5:
			JOptionPane.showMessageDialog(null,"Gracias por jugar!!");
			break;
			default: JOptionPane.showMessageDialog(null,"Esta opción no está disponible");
			}//Fin de switch
	}while(opcion!=5);
	}//Fin de menú
	
	public void jugar(){
		Pregunta pregunta;
		String datoLeido;
		int veces, i, respuesta;
		i=1;
		datoLeido=JOptionPane.showInputDialog("Digite la cantidad de preguntas que desea responder");
		veces=Integer.parseInt(datoLeido);
		while(i<=veces){
		
		pregunta = new Pregunta();
		datoLeido=JOptionPane.showInputDialog(pregunta.getEnunciado());
		respuesta=Integer.parseInt(datoLeido);
		estadistica.setRespuesta(respuesta);
		registrarRespuesta(pregunta,respuesta);
		i++;
		if(i==veces){
			estadistica.aumentarTotalPartidas();
			}//Fin de if5
			estadistica.getPromedio();
			estadistica.calcularPromedioTotal();
	}//Fin de while

	//preguntar al usuario cuantas preguntas desea contestar	
	}//Fin de jugar
	public void registrarRespuesta(Pregunta pregunta,int respuesta){
		double promedio;
		if(respuesta==pregunta.getResultado()){
			JOptionPane.showMessageDialog(null, estadistica.aumentarAcierto());
			estadistica.registrarPreguntaAcertada(pregunta);
			estadistica.aumentarTotalPreguntas();
			}
			else{
				JOptionPane.showMessageDialog(null,estadistica.aumentarFallo());
				estadistica.registrarPreguntaFallada(pregunta);
				estadistica.aumentarTotalPreguntas();
				}
		}//Fin de registrarRespuesta
	public void mostrarEstadistica(){
		JOptionPane.showMessageDialog(null,estadistica.toString());
		}
}//Fin de clase

