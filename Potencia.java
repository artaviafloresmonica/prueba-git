/*Resuelva la siguiente operación: Lea XY, donde X es la base y Y es el exponente.*/
import javax.swing.JOptionPane;
public class Potencia{
  public static void main(String arg[]){
    String datoLeido;
    int base, exponente, elemento, resultado;
    resultado=1;//Esta variable se inicializa porque va a acumular el resultado de las multiplicaciones
    elemento=0;
    datoLeido=JOptionPane.showInputDialog("Digite el valor de la base");
    base=Integer.parseInt(datoLeido);
    datoLeido=JOptionPane.showInputDialog("Digite el valor de la exponente");
    exponente=Integer.parseInt(datoLeido);
    while(elemento<exponente){
      resultado=resultado*base;
      elemento++;
    }//Fin del while
    JOptionPane.showMessageDialog(null,"El resultado de elevar "
                                    +base+" a "+exponente+"es "+resultado);
    
  }//Fin del main

}//Fin de la clase