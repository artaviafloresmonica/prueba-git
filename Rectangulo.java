public class Rectangulo{
 private int largo;
 private int ancho;
 
 public Rectangulo(int largo, int ancho){
	 this.largo=largo;
	 this.ancho=ancho;
	 }
 public Rectangulo(){}
 
 public void setLargo(int largo){
	 this.largo=largo;
	 }//Fin de setLargo
 public int getLargo(){
	 return largo;
	 }//Fin de getLargo
 public void setAncho(int ancho){
	 this.ancho=ancho;
	 }//Fin de setAncho}
 public int getAncho(){
	 return ancho;
	 }//Fin de getAncho
 public int perimetro(){
	 return ancho*2+largo*2;
	 }//Fin de perimetro
 public int area(){
	 return ancho*largo;
	 }//Fin de area
}//Fin de clase
