import javax.swing.JOptionPane;
public class MonRaq{
  public static void main (String arg[]){
    String datoLeido, titulo, genero, estudio;
    int numeroEjemplares, horasEstimadas,cantidadDeJuegos;
    char opcion;
    VideoJuego juego=null;
    do{
    datoLeido=JOptionPane.showInputDialog("******** VideoJuego MonRaq*******"
                                            +"\na. Incluir videojuego en oferta"
                                            +"\nb. Ver datos del videojuego"
                                            +"\nc. Vender videojuego"
                                            +"\nd. Ver cantidad disponibles de videojuegos"
                                            +"\ne. Salir");
    opcion=datoLeido.charAt(0);
    switch (opcion){
      case 'a':
        if(juego==null){
            titulo=JOptionPane.showInputDialog("Digite el t�tulo del videojuego");
            datoLeido=JOptionPane.showInputDialog("Digite la cantidad de horas estimadas");
            horasEstimadas=Integer.parseInt(datoLeido);
            datoLeido=JOptionPane.showInputDialog("Digite la cantidad de ejemplares disponibles");
            numeroEjemplares=Integer.parseInt(datoLeido);           
            genero=JOptionPane.showInputDialog("Digite el g�nero del videojuego");
            estudio=JOptionPane.showInputDialog("Digite el estudio del videojuego");
            juego=new VideoJuego(titulo,horasEstimadas,numeroEjemplares,genero,estudio);
      }else{
          JOptionPane.showMessageDialog(null,"Ya el juego ha sido incluido previamente");
      }
        break;
      case 'b':
        if(juego!=null){
         JOptionPane.showMessageDialog(null,juego.toString());
      }else{
         JOptionPane.showMessageDialog(null,"A�n no se ha ingresado un videojuego");
      }
        break;
      case 'c':
        if(juego!=null){
        datoLeido=JOptionPane.showInputDialog("�Cu�ntos ejemplares desea adquirir?");
        cantidadDeJuegos=Integer.parseInt(datoLeido);
        if(cantidadDeJuegos<=juego.getNumeroEjemplares()){
            juego.disminuir(cantidadDeJuegos);
            JOptionPane.showMessageDialog(null,"Muchas gracias por su compra");
        }else{
          JOptionPane.showMessageDialog(null,"No hay suficientes ejemplares para la venta ");
        }
      }
        else{
          JOptionPane.showMessageDialog(null,"No hay un videojuego registrado");
        }
        break;
      case 'd':
        if(juego!=null){
         JOptionPane.showMessageDialog(null,"Hay disponibles "+juego.getNumeroEjemplares());
        }
        else{
          JOptionPane.showMessageDialog(null,"No hay un videojuego registrado ");
        }
        break;
      case 'e':
        JOptionPane.showMessageDialog(null,"Es un gusto atenderle");
        break;
      default:JOptionPane.showMessageDialog(null,"Esa opci�n no est� disponible");
    
    }
    }while(opcion!='e');
  
  }

}